package com.example.dimas.vertuwkaacc;

/**
 * Created by Dimas on 21.03.18.
 */

public class CardStatistics {
    int _id;
    int _count;
    String _comment;
    String _date;

    public CardStatistics() {
    }

    public CardStatistics(int id, int count, String comment, String date) {
        this._id = id;
        this._count = count;
        this._comment = comment;
        this._date = date;
    }

    public CardStatistics(int count, String comment, String date) {
        this._count = count;
        this._comment = comment;
        this._date = date;
    }

    public int getID() {
        return this._id;
    }

    public void setID(int id) {
        this._id = id;
    }

    public int getCount() {
        return this._count;
    }

    public void setCount(int count) {
        this._count = count;
    }

    public String getComment() {
        return this._comment;
    }

    public void setComment(String comment) {
        this._comment = comment;
    }

    public String getDate() {
        return this._date;
    }

    public void setDate(String date) {
        this._date = date;
    }


}
