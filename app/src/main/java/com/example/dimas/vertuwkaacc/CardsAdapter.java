package com.example.dimas.vertuwkaacc;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Dimas on 21.03.18.
 */

public class CardsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<CardStatistics> statistics;
    private OnLoadMore mOnLoadMore;
    private boolean isLoading;
    private Context mContext;
    private LayoutInflater mLInflater;
    private RecyclerView mRecyclerView;

    private static final int ITEM_VIEW = 0;
    private static final int ITEM_LOADING = 1;

    private int totalItemCount;
    private int lastVisibleItem;
    private int visibleThreashold = 1;

    public CardsAdapter(Context context, List<CardStatistics> list,RecyclerView recyclerView) {
        mLInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mContext = context;
        statistics = list;
        mRecyclerView = recyclerView;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreashold)) {
                    isLoading = true;
                    if (mOnLoadMore != null) {
                        mOnLoadMore.onLoadMore();
                    }

                }

            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == ITEM_VIEW) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_view, parent, false);

            vh = new CardViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_progress_bar, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int getViewType = holder.getItemViewType();
        if (getViewType == ITEM_VIEW) {

            final CardStatistics card = statistics.get(position);
            CardViewHolder holder1 = (CardViewHolder) holder;
            holder1.title.setText("Успешно выполнено: " + String.valueOf(card.getCount()) + ". Ваш комментарий: " + card.getComment());
            holder1.pubDate.setText(card.getDate());
            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String body = "Успешно выполнено: " + String.valueOf(card.getCount()) + "\n" +
                            "Ваш коммент: " + card.getComment() + "\n" +
                            "Дата выполнения: " + card.getDate();
                    final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setTitle("Статистика тренировки");
                    builder.setMessage(body);
                    builder.setCancelable(false);
                    builder.setNegativeButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    builder.show();
                }
            });
        } else if (getViewType == ITEM_LOADING) {
            ProgressViewHolder progressHold = (ProgressViewHolder) holder;
            progressHold.mProgressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return statistics.get(position) != null ? ITEM_VIEW : ITEM_LOADING;
    }

    @Override
    public int getItemCount() {
        return (statistics != null) ? statistics.size() : 0;
    }


    public void setOnLoadMore(OnLoadMore onLoadMore) {
        mOnLoadMore = onLoadMore;
    }


    public void setIsLoading() {
        isLoading = false;
    }

    static class CardViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView pubDate;

        public CardViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            pubDate = (TextView) itemView.findViewById(R.id.pubDate);

        }

    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar mProgressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }
    }

    public interface OnLoadMore {
        public void onLoadMore();
    }

}
