package com.example.dimas.vertuwkaacc;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dimas on 21.03.18.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "userstore.db"; // название бд
    private static final int SCHEMA = 1; // версия базы данных
    static final String TABLE = "vertuwka"; // название таблицы в бд
    // названия столбцов
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_COUNT = "count";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_DATE = "date";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE vertuwka (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_COUNT + " INTEGER, "
                + COLUMN_COMMENT + " TEXT, "
                + COLUMN_DATE + " TEXT); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE);
        onCreate(db);
    }

    public void addTraining(CardStatistics cardStatistics) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseHelper.COLUMN_COUNT, cardStatistics.getCount());
        contentValues.put(DataBaseHelper.COLUMN_COMMENT, cardStatistics.getComment());
        contentValues.put(DataBaseHelper.COLUMN_DATE, cardStatistics.getDate());

        db.insert(DataBaseHelper.TABLE, null, contentValues);
        db.close();
    }

    public CardStatistics getStatistic(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE, new String[]{COLUMN_ID,
                        COLUMN_COUNT, COLUMN_COMMENT, COLUMN_DATE}, COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        CardStatistics cardStatistics = new CardStatistics(cursor.getInt(0), cursor.getInt(1), cursor.getString(2),cursor.getString(3));

        return cardStatistics;
    }

    public List<CardStatistics> getAllStatistics() {
        List<CardStatistics> cardStatisticsList = new ArrayList<CardStatistics>();
        String selectQuery = "SELECT  * FROM " + TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                CardStatistics cardStatistics = new CardStatistics();
                cardStatistics.setID(cursor.getInt(0));
                cardStatistics.setCount(cursor.getInt(1));
                cardStatistics.setComment(cursor.getString(2));
                cardStatistics.setDate(cursor.getString(3));
                cardStatisticsList.add(cardStatistics);
            } while (cursor.moveToNext());
        }

        return cardStatisticsList;
    }

    public int getStatisticsCount() {
        int count = 0;
        String countQuery = "SELECT  * FROM " + TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        if (cursor != null && !cursor.isClosed()) {
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }

}