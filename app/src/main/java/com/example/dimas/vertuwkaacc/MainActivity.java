package com.example.dimas.vertuwkaacc;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SensorEventListener {

    private SensorManager mSensorManager;

    private float y = 0;
    private float x = 0;
    private float z = 0;
    private float lastX = 0;
    private int count;
    private int countToComplete;

    private Timer timer;

    private TextView text;
    private TextView completedVertuwka;
    private TextView comment;

    private Button btnStart;
    private Button btnSave;

    private EditText countVertuwek;
    private EditText commentVertuwek;


    DataBaseHelper sqlHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        sqlHelper = new DataBaseHelper(this);


        text = (TextView) findViewById(R.id.text);
        completedVertuwka = (TextView) findViewById(R.id.completedVertuwek);

        comment = (TextView) findViewById(R.id.comment);
        comment.setVisibility(View.GONE);

        countVertuwek = (EditText) findViewById(R.id.countVertuwek);

        commentVertuwek = (EditText) findViewById(R.id.commentVertuwek);
        commentVertuwek.setVisibility(View.GONE);

        btnSave = (Button) findViewById(R.id.saveVertuwka);
        btnSave.setVisibility(View.GONE);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToDB();
            }
        });

        btnStart = (Button) findViewById(R.id.startVertuwka);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countVertuwek.getText().length() > 0) {
                    countToComplete = Integer.parseInt(countVertuwek.getText().toString());
                    if (btnStart.getText().equals("Начать упражнение")) {
                        vertuwkaStart();
                    } else {
                        vertuwkaStop();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Введите количество вращений", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (btnStart.getText().equals("Закончить упражнение")) {
            vertuwkaStop();
        }
    }

    private void vertuwkaStop() {
        mSensorManager.unregisterListener(this);
        timer.cancel();
        btnStart.setText("Начать упражнение");
        countVertuwek.setText(null);
        countVertuwek.setVisibility(View.VISIBLE);
        commentVertuwek.setVisibility(View.VISIBLE);
        text.setVisibility(View.VISIBLE);
        btnSave.setVisibility(View.VISIBLE);
        comment.setVisibility(View.VISIBLE);

    }

    private void vertuwkaStart() {
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
        btnStart.setText("Закончить упражнение");

        countVertuwek.setVisibility(View.GONE);
        text.setVisibility(View.GONE);
        btnSave.setVisibility(View.GONE);
        commentVertuwek.setVisibility(View.GONE);
        comment.setVisibility(View.GONE);
        completedVertuwka.setText("0");

        count = 0;

        timer = new Timer(); //вариант использования ТОЛЬКО акселерометра,если можно, то хотелось бы увидеть более красивое решение, чтобы чему-то научиться!
        TimerTask task = new TimerTask() {// в моем решении есть дырка, как закрыть пока не решил.
            @Override
            public void run() {
                if ((z < 9.7)) {
                    if (y < 0) {
                        if (((lastX + x) <= 9) & ((lastX + x) >= -9)) {
                            count++;
                        }
                        lastX = x;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Возьмите телефон в руку и вытяните перед собой", Toast.LENGTH_SHORT).show();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                                    completedVertuwka.setText(String.valueOf(count) + " из " + String.valueOf(countToComplete));
                    }
                });
            }
        };
        timer.schedule(task, 0, 200);
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    private void saveToDB() {
        sqlHelper.addTraining(new CardStatistics(count, commentVertuwek.getText().toString(), getDateTime()));
        Toast.makeText(this, "Сохранено", Toast.LENGTH_SHORT).show();
        btnSave.setVisibility(View.GONE);
        commentVertuwek.setVisibility(View.GONE);
        commentVertuwek.setText("");
        comment.setVisibility(View.GONE);
        completedVertuwka.setText("0");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        y = event.values[1];
        if (y > -3) {
            x = event.values[0];
        }
        z = Math.abs(event.values[2]);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_training) {
        } else if (id == R.id.nav_statistics) {
            Intent intent = new Intent(this, RecyclerActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
