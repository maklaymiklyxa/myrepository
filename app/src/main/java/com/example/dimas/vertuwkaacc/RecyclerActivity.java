package com.example.dimas.vertuwkaacc;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dimas on 21.03.18.
 */

public class RecyclerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView cardsRecycler;
    CardsAdapter adapter;
    private List<CardStatistics> mItems = new ArrayList<>();
    DataBaseHelper databaseHelper;
    int count = 1;
    int rangeOfLoad = 5;
    int sum = 5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_recycler);
        setSupportActionBar(toolbar);

        databaseHelper = new DataBaseHelper(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_card);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        cardsRecycler = (RecyclerView) findViewById(R.id.cards_recycler);
        cardsRecycler.setHasFixedSize(false);
        cardsRecycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CardsAdapter(this, mItems, cardsRecycler);
        cardsRecycler.setAdapter(adapter);
        cardsRecycler.setItemAnimator(new DefaultItemAnimator());
        if (databaseHelper.getStatisticsCount() > 0) {
            for (; count <= sum; count++) {// rangeOfLoad определяет количество pull to load
                if (count <= databaseHelper.getStatisticsCount()) {
                    mItems.add(databaseHelper.getStatistic(count));
                } else {
                    Toast.makeText(getApplicationContext(), "Данных больше нет", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            adapter.setOnLoadMore(new CardsAdapter.OnLoadMore() {
                @Override
                public void onLoadMore() {
                    mItems.add(null);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mItems.remove(mItems.size() - 1);
                            for (; count <= sum; count++) {// rangeOfLoad определяет количество pull to load
                                if (count <= databaseHelper.getStatisticsCount()) {
                                    mItems.add(databaseHelper.getStatistic(count));
                                } else {
                                    Toast.makeText(getApplicationContext(), "Данных больше нет", Toast.LENGTH_SHORT).show();
                                    break;
                                }
                            }
                            adapter.notifyDataSetChanged();
                            sum = rangeOfLoad + count;
                            adapter.notifyDataSetChanged();
                            adapter.setIsLoading();
                        }
                    }, 2000);


                }
            });
        } else {
            Toast.makeText(this, "Ваша статистика пуста", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_card);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_training) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_statistics) {
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_card);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseHelper.close();
    }
}
